import axios from 'axios';


export default {

  checkToken: data =>
    axios({
      method: 'get',
      url: `${data.apiUrl}/projects?private_token=${data.token}`,
    }).then(res => res),
};
