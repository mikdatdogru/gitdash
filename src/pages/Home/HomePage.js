import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return null;
  }

  render() {
    return (
      <div>
        <FormattedMessage id="general.helloworld" defaultMessage="Anasayfa" />
      </div>
    );
  }
}

HomePage.propTypes = {};
HomePage.defaultProps = {};

const mapStateToProps = state => ({});
const mapDispatchToProps = {};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
