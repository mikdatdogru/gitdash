import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Button, Form, Header, Icon, Message, Modal, Popup } from 'semantic-ui-react';
import { control } from '../../utils/form';

import { checkToken } from '../../actions/common';
import FormField from '../../components/FormField';
import { authControl } from '../../utils/helper';

const { basePath } = window.env;

class Login extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.setGitLab = this.setGitLab.bind(this);

    this.state = {
      error: {},
      apiUrl: '',
      token: '',
      redirectToReferrer: false,
    };
  }

  static getDerivedStateFromProps(nextProps) {
    if (!authControl()) return null;
    return {
      redirectToReferrer: true,
    };
  }

  setGitLab= () => {
    this.setState({
      apiUrl:"https://gitlab.com/api/v4",
    });
  }
  submit = () => {
    const { apiUrl, token } = this.state;

    this.props.checkToken({ apiUrl, token });
  };

  render() {
    let locationState = this.props.location.state;
    const { redirectToReferrer } = this.state;

    if (!locationState) {
      locationState = {
        from: {
          pathname: `${basePath}/`,
        },
      };
    }

    if (redirectToReferrer) {
      return <Redirect to={locationState.from} />;
    }

    const { error } = this.state;
    const style = {
      borderRadius: "4px",
      opacity: 1,
      padding: '2px 4px',
      background:"white",
    }
    return (
      <div>
        <Modal open onClose={this.handleClose} basic size="small">
          <Header icon="unlock alternate" content="Please Log In" />
          <Modal.Content>
            <Form autoComplete="off">
              <label htmlFor="apiUrl" className="d-flex justify-content-between">
                Api Url{' '}
                <small>
                  <strong>For Gitlab: </strong>

                  <Popup
                    trigger={<a className="pointer" onClick={this.setGitLab}>https://gitlab.com/api/v4</a>
                    }
                    size='mini'

                    style={style}
                    position='top center'
                    content='Select this'
                    on={['hover', 'click']}
                  />

                </small>
              </label>

              <FormField type="text" {...control(this, 'apiUrl')} />

              <label htmlFor="token" className="d-flex justify-content-between">
                {' '}
                Personal access token{' '}
                <small>
                  <strong>Example: </strong>Eg. MHv-PzXbZ1UZ-iqHVe9i
                </small>
              </label>
              <FormField type="text" {...control(this, 'token')} />
            </Form>

            <p>
              <a
                className="text-center d-flex justify-content-center"
                href="https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html"
                target="_blank"
                rel="noreferrer noopener"
              >
                How can i create Personal access token ?
              </a>
            </p>

            {error.message && (
              <Message negative>
                <Message.Header>{error.message}</Message.Header>
                <p>Please Check your API Url and Personal access token</p>
              </Message>
            )}
          </Modal.Content>
          <Modal.Actions>
            <Button
              loading={this.props.token.isFetching}
              color="green"
              inverted
              onClick={this.submit}
            >
              <Icon name="checkmark" /> Save
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

Login.propTypes = {
  location: PropTypes.objectOf(PropTypes.any),

  checkToken: PropTypes.func.isRequired,
  token: PropTypes.objectOf(PropTypes.any).isRequired,
};
Login.defaultProps = {
  location: { from: { pathname: '/' } },
};
const mapStateToProps = state => ({
  apiUrl: state.apiUrl,
  token: state.checkToken,
  language: state.language,
});
const mapDispatchToProps = {
  checkToken,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
