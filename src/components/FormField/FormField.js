import React from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import moment from 'moment';

import { FormGroup, Input, InputGroup, Label } from 'reactstrap';

let newId = 0;

const TextInput = props => (
  <InputGroup size="sm">
    {props.addonLeft && (
      <div className="input-group-prepend">
        <span className="input-group-text">
          {typeof props.addonLeft === 'boolean' ? <i className={props.icon} /> : props.addonLeft}
        </span>
      </div>
    )}

    <Input
      {..._.omit(props, ['error', 'hasRequired', 'value', 'addonLeft', 'addonRight', 'icon', 'id'])}
      value={props.value || ''}
      onChange={evt => {
        props.onChange(evt.target.value);
      }}
      className={`${props.error && 'inputError'}`}
    />

    {props.addonRight && (
      <div className="input-group-prepend">
        <span className="input-group-text">
          {typeof props.addonRight === 'boolean' ? <i className={props.icon} /> : props.addonRight}
        </span>
      </div>
    )}
  </InputGroup>
);
TextInput.propTypes = {
  icon: PropTypes.string,
  addonLeft: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
  ]),
  addonRight: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
  ]),
  onChange: PropTypes.func.isRequired,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  value: PropTypes.string,
  error: PropTypes.string,
};
TextInput.defaultProps = {
  icon: '',
  addonLeft: null,
  addonRight: null,
  name: '',
  value: '',
  error: '',
};

const Textarea = props => (
  <InputGroup size="sm">
    <Input
      type="textarea"
      rows="4"
      {..._.omit(props, ['error', 'hasRequired', 'value', 'addonLeft', 'addonRight'])}
      value={props.value}
      onChange={evt => {
        props.onChange(evt.target.value);
      }}
    />
  </InputGroup>
);
Textarea.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  value: PropTypes.string,
  error: PropTypes.string,
};
Textarea.defaultProps = {
  placeholder: '',
  name: '',
  value: '',
  error: '',
};

const FormField = props => {
  let Component;

  switch (props.type) {
    case 'textarea':
      Component = Textarea;
      break;

    default:
      Component = TextInput;
  }

  const inputProps = _.omit(props, ['statusColor', 'iconAlign', 'contClass']);
  const name = props.name ? props.name : (newId += 1);
  const id = props.id || props.name;
  inputProps.id = id;
  inputProps.name = name;

  return (
    <FormGroup className={props.contClass}>
      {props.label && <Label for="exampleEmail">{props.label}</Label>}
      <Component {...inputProps} />
      {props.error && <span className="formError font-italic c-red">{props.error}</span>}
    </FormGroup>
  );
};

FormField.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.oneOfType([PropTypes.object, PropTypes.string, PropTypes.number]),
  error: PropTypes.string,
  contClass: PropTypes.string,
};
FormField.defaultProps = {
  name: '',
  id: '',
  label: '',
  error: '',
  contClass: '',
};

export default FormField;
