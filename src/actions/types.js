export const LOCALE_SET = 'LOCALE_SET';
export const LOCALE_GET = 'LOCALE_GET';

export const CHECK_TOKEN_FETCH = 'CHECK_TOKEN_FETCH';
export const CHECK_TOKEN_RECEIVE = 'CHECK_TOKEN_RECEIVE';
export const CHECK_TOKEN_FAILURE = 'CHECK_TOKEN_FAILURE';

export const API_URL = 'API_URL';
export const TOKEN = 'TOKEN';
export const LOGOUT = 'LOGOUT';
