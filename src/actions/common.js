import { toast } from 'react-toastify';
import {
  LOCALE_SET,
  CHECK_TOKEN_FETCH,
  CHECK_TOKEN_RECEIVE,
  CHECK_TOKEN_FAILURE,
  API_URL,
  TOKEN,
  LOGOUT,
} from './types';
import { localStorageData } from '../utils/helper';
import api from '../utils/api';
import {history} from '../index';
import setAuthorizationToken from '../utils/setAuthorizationToken';

const { basePath } = window.env;

export function localeSet(lang) {
  return {
    type: LOCALE_SET,
    lang,
  };
}

export const setLocale = lang => dispatch => {
  localStorageData.set('language', lang);
  dispatch(localeSet(lang));
};

export const checkTokenFetch = data => ({
  type: CHECK_TOKEN_FETCH,
  data,
});

export const checkTokenReceive = data => ({
  type: CHECK_TOKEN_RECEIVE,
  data,
});
export const checkTokenFailure = data => ({
  type: CHECK_TOKEN_FAILURE,
  data,
});

export const setApiUrl = data => ({
  type: API_URL,
  data,
});

export const setToken = data => ({
  type: TOKEN,
  data,
});

export function checkToken(data) {
  return dispatch => {
    dispatch(checkTokenFetch(data));
    return api
      .checkToken(data)
      .then(res => {
        dispatch(checkTokenReceive(res.data));

        localStorageData.set('apiUrl', data.apiUrl);
        localStorageData.set('token', data.token);

        dispatch(setToken(data.token));
        dispatch(setApiUrl(data.apiUrl));

        toast.info('Saved!');

        return res;
      })
      .catch(err => {
        dispatch(checkTokenFailure(err.response));

        return err;
      });
  };
}

export const doLogout = data => ({
  type: LOGOUT,
  data,
});

export function logout(location) {
  return dispatch => {
    localStorageData.delete('token');
    localStorageData.delete('apiUrl');

    setAuthorizationToken(false);


    dispatch(setApiUrl({}));
    dispatch(setToken({}));
    dispatch(checkTokenFailure({}));
    dispatch(doLogout(dispatch));

    history.push(`${basePath}/account/login`, {from:location});

  };
}
