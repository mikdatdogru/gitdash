import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';
import TopNavigation from '../Layouts/TopNavigation';
import Home from '../../pages/Home';

const { basePath } = window.env;

class MainLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <TopNavigation />
        <Switch>
          <Route exact path={`${basePath}/`} component={Home} />
          {/* <Redirect to={`${basePath}/404`} /> */}
        </Switch>
      </div>
    );
  }
}

export default MainLayout;
