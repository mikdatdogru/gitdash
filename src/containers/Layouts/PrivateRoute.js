import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { authControl } from '../../utils/helper';

const { basePath } = window.env;

// auth kontorlu burada yapilacak


const PrivateRoute = ({ component: Component, ...rest }) => {

  const auth = authControl();


  return (
    <Route
      {...rest}
      render={props =>
        auth ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: `${basePath}/account/login`,
              state: { from: props.location },
            }}
          />
        )
      }
    />
  )
};

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.objectOf(PropTypes.any),
};
PrivateRoute.defaultProps = {
  location: { from: { pathname: `${basePath}/` } },
};

const mapStateToProps = state => ({

});
const mapDispatchToProps = {
  
};
export default connect(mapStateToProps,mapDispatchToProps)(PrivateRoute);
