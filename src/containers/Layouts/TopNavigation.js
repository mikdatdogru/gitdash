import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Dropdown, Menu } from 'semantic-ui-react';
import { Flag } from '../../components/common';
import { store } from '../../index';
import { setLocale, logout } from '../../actions/common';

class TopNavigation extends Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);

    this.state = {};
  }

  handleItemClick = (e, { name }) => {
    this.setState({ activeItem: name });
  };

  logout = () => {
    this.props.logout(this.props.location);
  };

  languageChanger(lang) {
    store.dispatch(setLocale(lang));
  }

  render() {
    const { lang } = this.props;
    const { activeItem } = this.state;
    return (
      <div>
        <Menu stackable>
          <Menu.Item>
            <img src="/img/icon-128.png" />
          </Menu.Item>

          <Menu.Item
            name="dashboard"
            active={activeItem === 'dashboard'}
            onClick={this.handleItemClick}
          >
            Dashboard
          </Menu.Item>

          <Menu.Item
            name="allissues"
            active={activeItem === 'allissues'}
            onClick={this.handleItemClick}
          >
            All Issues
          </Menu.Item>

          <Menu.Menu position="right">
            <Menu.Item name="language" onClick={this.handleItemClick}>
              <Dropdown pointing trigger={<Flag name={lang} />}>
                <Dropdown.Menu>
                  <Dropdown.Item
                    selected={lang === 'tr'}
                    onClick={() => this.languageChanger('tr')}
                  >
                    <Flag name="tr" />
                  </Dropdown.Item>
                  <Dropdown.Item
                    selected={lang === 'en'}
                    onClick={() => this.languageChanger('en')}
                  >
                    <Flag name="us" />
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </Menu.Item>
            <Menu.Item
              name="settings"
              active={activeItem === 'settings'}
              onClick={this.handleItemClick}
            >
              Settings
            </Menu.Item>

            <Menu.Item name="logout" onClick={this.logout}>
              Logout
            </Menu.Item>
          </Menu.Menu>
        </Menu>
      </div>
    );
  }
}

TopNavigation.propTypes = {
  lang: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  lang: state.locale.lang,
});
const mapDispatchToProps = {
  logout,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TopNavigation);
