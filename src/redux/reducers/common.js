import {
  CHECK_TOKEN_FETCH,
  CHECK_TOKEN_RECEIVE,
  CHECK_TOKEN_FAILURE,
  API_URL,
  TOKEN,
} from '../../actions/types';

const initialState = {
  isFetching: false,
  isLoaded: false,
  isFailure: false,
};

export const x = () => {};

export const checkToken = (state = initialState, action = {}) => {
  switch (action.type) {
    case CHECK_TOKEN_FETCH:
      return {
        ...state,
        isFetching: true,
        isFailure: false,
        isLoaded: false,
        data: action.data,
      };

    case CHECK_TOKEN_RECEIVE:
      return {
        ...state,
        isFetching: false,
        isFailure: false,
        isLoaded: true,
        data: action.data,
      };

    case CHECK_TOKEN_FAILURE:
      return {
        ...state,
        isFetching: false,
        isFailure: true,
        isLoaded: false,
        data: action.data,
      };

    default:
      return state;
  }
};

export const apiUrl = (state = initialState, action = {}) => {
  switch (action.type) {
    case API_URL:
      return action;
    default:
      return state;
  }
};
export const token = (state = initialState, action = {}) => {
  switch (action.type) {
    case TOKEN:
      return action;
    default:
      return state;
  }
};
