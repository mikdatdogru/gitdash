import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import locale from './reducers/locale';
import { checkToken, token, apiUrl } from './reducers/common';

export default combineReducers({
  router: routerReducer,
  locale,
  checkToken,
  token,
  apiUrl,
});
