import { injectGlobal } from 'styled-components';

// eslint-disable-next-line
injectGlobal`
  html{
  }
  body{
    background: #f0f4f7;

  }
`;

export default {
  primaryColor: '#63acff',
  secondaryColor: '#9bff69',
};
